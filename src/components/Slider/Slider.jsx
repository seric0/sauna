import React, {useState} from "react";
import { Swiper, SwiperSlide } from "swiper/react";

import slider from "./Slider.module.scss"

import sauna_1 from "../../images/rahat_monsha_kst_289440087_161862403039398_1275083599485214900_n.jpg";
import sauna_2 from "../../images/rahat_monsha_kst_293983799_5861117353902684_7152303149142147368_n.jpg";
import sauna_3 from "../../images/rahat_monsha_kst_297630341_763154131678192_5130327974426080735_n.jpg";
import sauna_4 from "../../images/rahat_monsha_kst_301622335_492955289308212_4078488914212272371_n.jpg";
import sauna_5 from "../../images/rahat_monsha_kst_306186051_1175699793009150_8458880923204856877_n.jpg";
import sauna_6 from "../../images/rahat_monsha_kst_310546870_1889637004568159_5783997976374786973_n.jpg";
import sauna_7 from "../../images/rahat_monsha_kst_310874436_8803483752995826_3048342853679885312_n.jpg";
import sauna_8 from "../../images/rahat_monsha_kst_312813380_470857458194751_6234969572314559245_n.jpg";
import sauna_9 from "../../images/rahat_monsha_kst_314664077_182637984309221_2571196366385995452_n.jpg";
import sauna_10 from "../../images/rahat_monsha_kst_322280170_209813604857636_5989886389584043957_n.jpg";
import sauna_11 from "../../images/rahat_monsha_kst_325608538_1606018609880132_6112047807564769984_n.jpg";
import sauna_12 from "../../images/rahat_monsha_kst_145.jpg";
import left from "../../images/icons8-left.png";
import right from "../../images/icons8-right.png";

import "swiper/css";
import "swiper/css/navigation";

function Slider() {
  const [swiper, setSwiper] = useState(null);

  const slide = (isNext) => {    
  if (isNext) {
    swiper.slideNext();
   } else {
      swiper.slidePrev();
    }
  };
  
  return (
        <div className={slider.container}>
        <h2 className={slider.headText}>Лучшие фото бань и саун</h2>
        <div className={slider.sliderButtons}>          
          <button type="button" className={slider.sliderButton} onClick={(event) => slide(false)}>
          <img src={left} alt="<="/>          
            </button>
          <button type="button" className={slider.sliderButton} onClick={(event) => slide(true)}>
          <img src={right} alt="=>"/>          
          </button>
        </div>
        <Swiper onSwiper={(instance) => setSwiper(instance)} className={setSwiper.Swiper} spaceBetween={10}>
        <SwiperSlide><img src={sauna_1} alt="Photo sauna Raxat" className={slider.fill}/><div className={slider.imageText}>Баня</div></SwiperSlide>
        <SwiperSlide><img src={sauna_2} alt="Photo sauna Raxat" className={slider.fill}/><div className={slider.imageText}>Баня</div></SwiperSlide>
        <SwiperSlide><img src={sauna_3} alt="Photo sauna Raxat" className={slider.fill}/ ><div className={slider.imageText}>Баня</div></SwiperSlide>
        <SwiperSlide><img src={sauna_4} alt="Photo sauna Raxat" className={slider.fill}/><div className={slider.imageText}>Баня</div></SwiperSlide>        
        <SwiperSlide><img src={sauna_5} alt="Photo sauna Raxat" className={slider.fill}/><div className={slider.imageText}>Баня</div></SwiperSlide>        
        <SwiperSlide><img src={sauna_6} alt="Photo sauna Raxat" className={slider.fill}/><div className={slider.imageText}>Баня</div></SwiperSlide>        
        <SwiperSlide><img src={sauna_7} alt="Photo sauna Raxat" className={slider.fill}/><div className={slider.imageText}>Баня</div></SwiperSlide>        
        <SwiperSlide><img src={sauna_8} alt="Photo sauna Raxat" className={slider.fill}/><div className={slider.imageText}>Баня</div></SwiperSlide>        
        <SwiperSlide><img src={sauna_9} alt="Photo sauna Raxat" className={slider.fill}/><div className={slider.imageText}>Баня</div></SwiperSlide>        
        <SwiperSlide><img src={sauna_10} alt="Photo sauna Raxat" className={slider.fill}/><div className={slider.imageText}>Баня</div></SwiperSlide>        
        <SwiperSlide><img src={sauna_11} alt="Photo sauna Raxat" className={slider.fill}/><div className={slider.imageText}>Баня</div></SwiperSlide>        
        <SwiperSlide><img src={sauna_12} alt="Photo sauna Raxat" className={slider.fill}/><div className={slider.imageText}>Баня</div></SwiperSlide>        
      </Swiper>
      </div>
    );
}

export default Slider;
