import React, {useState, useEffect} from "react";

import s from "./NewsGrid.module.scss";

import NewsCard from "../TennisCard";

const endpoint = "http://localhost:3001"

function NewsGrid() {
    const [posts, setPosts] = useState([])
    useEffect(() => {
      const fetchData = async () => {
        const res = await fetch(`${endpoint}/posts`)
        const data = await res.json()
        setPosts(data)
      }
      fetchData()
    }, [])
  return (
    <div className={s.newsGrid}>
    <div className={s.container}>
      <div className={s.grid}>
        {posts.map((elem) => (
          <NewsCard
            img={elem.image}
            text={elem.title}
            key={elem.id}
            id={elem.id}
          />
        ))}
      </div>
    </div>
  </div>
  );
}

export default NewsGrid;
