import React from "react";

import image_photo_1 from "../../images/man_logo.png";
import image_photo_2 from "../../images/man_logo.png";
import image_photo_3 from "../../images/man_logo.png";

import info from "./AboutInfo.module.scss"

function AboutInfo() {
    return (
        <div className={info.mainContainer}>
        <h2 className={info.main_text}>Агрегатор бань и саун</h2>
 <div className={info.row}>
  <div className={info.column}>
    <div className={info.card}>
      <img src={image_photo_1} alt="Jane" className={info.pict} />
      <div className={info.container}>
        <h2 className={info.text}>Баня "Рахат"</h2>
        <p className={info.title}>Лучший банный комплекс</p>
        <p className={info.info}>Проект создан для объеденения ценителей и любителей бани и сауны. Это место, где переплетаются технологичность и традиции.</p>
        <p className={info.contact}>Сайт: seric0.kz</p>        
      </div>
    </div>
  </div>

  <div className={info.column}>
    <div className={info.card}>
      <img src={image_photo_2} alt="Mike" className={info.pict} />
      <div className={info.container}>
        <h2 className={info.text}>VIP сауна "Лед"</h2>
        <p className={info.title}>Одна из лучших саун</p>
        <p className={info.info}>Агрегатор бань и саун для пользователя - это платформа с отборными банными комплексами города Костанай.</p>
        <p className={info.contact}>Почта: seric0@mail.ru</p>        
      </div>
    </div>
  </div>
  
  <div className={info.column}>
    <div className={info.card}>
      <img src={image_photo_3} alt="John" className={info.pict} />
      <div className={info.container}>
        <h2 className={info.text}>Баня-сауна " Эврика"</h2>
        <p className={info.title}>И баня, и сауна</p>
        <p className={info.info}>Агрегатор бань и саун предназначен для просмотра, поиска нужно бани и сауны города Костанай.</p>
        <p className={info.contact}>What'up: 8 (XXX) XXX-XX-XX</p>        
      </div>
    </div>
  </div>
</div>
         </div>
    );
}

export default AboutInfo;
