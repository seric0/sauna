import React from "react";

import stock from "./StockCard.module.scss"

import { Link } from "react-router-dom";

import SaunaCard_5 from "../../images/saunaCard_5.png";
import SaunaCard_6 from "../../images/saunaCard_6.png";
import SaunaCard_7 from "../../images/saunaCard_7.png";
import SaunaCard_8 from "../../images/saunaCard_8.png";

function StockCard() {
  const stockscard = [
    {
      id: 42,
      img: SaunaCard_5,
      text: "Баня Рахат",
      price: '1500'
    },
    {
      id: 13,
      img: SaunaCard_6,
      text: "Баня на Кооперативной",
      price: '1500'
    },
    {
      id: 11,
      img: SaunaCard_7,
      text: "Баня сауна Аксу",
      price: '1500'
    },
    {
      id: 9,
      img: SaunaCard_8,
      text: "Баня klub",
      price: '1500'
    } 
  ]
    return (    
      stockscard.map((homecard) => (  
        <div className={stock.card}>    
      <div className={stock.imageWrapper}>
        {/* <img className={stock.fill} src={homecard.img} alt="" /> */}
        <Link key={homecard.id} to={`/all/${homecard.id}`} className={stock.text}><img src={homecard.img} alt="" /></Link>
      </div>
      {/* <div className={s.text}>{homecard.text}</div> */}
      <Link key={homecard.id} to={`/all/${homecard.id}`} className={stock.text}>{homecard.text}</Link>
      <div className={stock.price}>от {homecard.price} тенге</div>
    </div> 
    ))  
    );
}

export default StockCard;
