import React, { useState } from "react";

import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { savePost } from "../../store/postSlice";

import card from "./PanelCard.module.scss";

function PanelCard() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [name, setName] = useState("");  
  const [price, setPrice] = useState("");  
  const [description, setDiscription] = useState("");
  const [detailed_description, setfullDiscription] = useState(""); 
  const [region, setRegion] = useState("");
  const [address, setAddress] = useState("");
  const [number_halls, setHalls] = useState("");
  const [number_people, setPeople] = useState("");
  const [types_sauna, setSauna] = useState("");
  const [work_time, setWork] = useState("");
  const [thumbnail, setPhoto] = useState("");
  const [category_id, setCategory] = useState("");
  const [phone, setPhone] = useState("");
  const onSubmit = (e) => {
    e.preventDefault();

    dispatch(savePost({ name, price, description, detailed_description, region, number_halls, number_people, types_sauna, work_time, thumbnail, category_id })).then((res) => {       
      navigate("/all");
    }
    )};
  return (
    <div className={card.Myform}>
      <h1>Сохранение данных</h1>
      <div className={card.inp}>
      <form onSubmit={onSubmit}>
      <div className={card.group_name}>
      <input className={card.name} type="text" required value={name} placeholder="Наименование" onChange={(e) => setName(e.target.value)}/>
      <input className={card.name} type="number" required value={price} placeholder="Цена" onChange={(e) => setPrice(e.target.value)}/>
      </div>
      <div className={card.group_name}>
      <textarea className={card.discription} required value={description} placeholder="Краткое описание" onChange={(e) => setDiscription(e.target.value)}/>
      <textarea className={card.discription} required value={detailed_description} placeholder="Полное описание" onChange={(e) => setfullDiscription(e.target.value)}/>
      </div>
      <div className={card.group_name}>
      <input className={card.name} type="text" required value={region} placeholder="Регион" onChange={(e) => setRegion(e.target.value)}/>
      <input className={card.name} type="text" required value={address} placeholder="Адрес" onChange={(e) => setAddress(e.target.value)}/>
      </div>
      <div className={card.group_name}>
      <input className={card.name} type="text" required value={number_halls} placeholder="Кол-во залов" onChange={(e) => setHalls(e.target.value)}/>
      <input className={card.name} type="text" required value={number_people} placeholder="Кол-во людей" onChange={(e) => setPeople(e.target.value)}/>
      </div>
      <div className={card.group_name}>
      {/* <input className={card.name} type="text" required value={types_sauna} placeholder="Тип бани/сауны" onChange={(e) => setSauna(e.target.value)}/> */}
      <select className={card.name} onChange={(e) => setSauna(e.target.value)}>
                    <option value={types_sauna}>Русская</option>
                    <option value={types_sauna}>Финская</option>
                    <option value={types_sauna}>Турецкая</option>
                    <option value={types_sauna}>Японская</option>
                </select>
      <input className={card.name} type="text" required value={work_time} placeholder="Время работы" onChange={(e) => setWork(e.target.value)}/>
      </div>
      <div className={card.group_name}>
      <input className={card.name} type="text" required value={thumbnail} placeholder="Основное фото" onChange={(e) => setPhoto(e.target.value)}/>
      <input className={card.name} type="text" required value={category_id} placeholder="Категория" onChange={(e) => setCategory(e.target.value)}/>
      </div>
      <div className={card.group_name}>
      <input className={card.name} type="text" required value={phone} placeholder="Телефоны" onChange={(e) => setPhone(e.target.value)}/>      
      </div>
      {/* <input className={card.pass} type="password" required value={password} placeholder="пароль" /> */}
      <button type="submit" className={card.btn}>Сохранить</button>      
      </form>   
      </div>
    </div>    
  );
}

export default PanelCard;
