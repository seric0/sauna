import React from "react";

import footer from "./Footer.module.scss"

import YoutubeImage from "../../images/youtube.svg";

import VkImage from "../../images/vk.svg";

import planeImage from "../../images/paper-plane-solid.svg";

function Footer(props) {
  return (
    <footer>
      <div className={footer.container}>
        <span className={footer.copyright}>2023 © Все права защищены.</span>
        <nav>
          <ul>
            <li>
              <a href="/"> {props.menu_1}</a>
            </li>
            <li>
              <a href="/all"> {props.menu_2}</a>
            </li>
            <li>
              <a href="/gallery"> {props.menu_3}</a>
            </li>
            <li>
              <a href="/about"> {props.menu_4}</a>
            </li>
            <li>
              <a href="#"> {props.menu_5}</a>
            </li>            
          </ul>
          {/* <div className={footer.icons}>
            <img className="icon" src={YoutubeImage} alt="dots icon" />
            <img className="icon" src={VkImage} alt="dots icon" />
            <img className="icon" src={planeImage} alt="dots icon" />            
          </div> */}
        </nav>
      </div>
    </footer>
  );
}

export default Footer;
