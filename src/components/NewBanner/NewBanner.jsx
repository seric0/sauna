import React from "react";

import newbanner from "./NewBanner.module.scss"

function NewBanner() {
    return (        
      <div className={newbanner.innerHeader}>
      <h1 className={newbanner.main__head}>Лучшие бани и сауны только у нас</h1>
      </div>      
    );
}

export default NewBanner;