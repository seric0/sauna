import React, { useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";

import service from "./HomeServices.module.scss"

// import leftArrow from "../../images/arrow-left-solid.svg";
// import rightArrow from "../../images/arrow-right-solid.svg";

import services_1 from "../../images/services_1.png";
import services_2 from "../../images/services_2.png";
import services_3 from "../../images/services_3.png";

import "swiper/css";
import { Parallax } from "swiper";

const uslname = ["Уютное кафе", "Бассейн", "Тренажерный зал"];

function HomeServices() {
    const [swiper, setSwiper] = useState(null);

    const slide = (isNext) => {
        if (isNext) {
            swiper.slideNext();
        } else {
            swiper.slidePrev();
        }
    };
    return (
        <div className={service.serviceBottom}>
            <div className={service.container}>
                <div className={service.box}>
                    <h3>Цены и абонементы</h3>
                    <div className={service.boxCard}>
                        <div className={service.box1}>
                            <p>Зимний сезон 2021-2022</p>
                            {/* <img className="icon" src={leftArrow} alt="dots icon" /> */}
                        </div>
                        <div className={service.box1}>
                            <p>Абонементы</p>
                            {/* <img className="icon" src={leftArrow} alt="dots icon" /> */}
                        </div>
                        <div className={service.box1}>
                            <p>Скидки</p>
                            {/* <img className="icon" src={leftArrow} alt="dots icon" /> */}
                        </div>
                        <div className={service.box1}>
                            <p>Дополнительные услуги</p>
                            {/* <img className="icon" src={leftArrow} alt="dots icon" /> */}
                        </div>
                    </div>
                </div>
                <div className={service.box}>
                    <nav>
                        <h3>Услуги</h3>
                        <div className={service.btn}>
                            <button type="button" className={service.sliderButton} onClick={(event) => slide(false)}>
                                {"<="}
                            </button>
                            <button type="button" className={service.sliderButton} onClick={(event) => slide(true)}>
                                {"=>"}
                            </button>
                            {/* <i class="fa-solid fa-arrow-left"></i>
                        <i class="fa-solid fa-arrow-right"></i> */}
                            {/* <img className={service.icon} src={leftArrow} alt="dots icon" />
                        <img className={service.icon} src={rightArrow} alt="dots icon" /> */}
                        </div>
                    </nav>
                    <div className={service.image}>                        
                        {/* <img src={services_1} alt="" /> */}
                        <Swiper onSwiper={(instance) => setSwiper(instance)} className={setSwiper.Swiper} spaceBetween={10} parallax={true} modules={[Parallax]}>
                            <SwiperSlide><div className={service.imageText}>{uslname[0]}</div><img src={services_1} alt="" /></SwiperSlide>
                            <SwiperSlide><div className={service.imageText}>{uslname[1]}</div><img src={services_2} alt="" /></SwiperSlide>
                            <SwiperSlide><div className={service.imageText}>{uslname[2]}</div><img src={services_3} alt="" /></SwiperSlide>
                        </Swiper>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default HomeServices;
