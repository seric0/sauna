import React from "react";

import banner from "./HomeBanner.module.scss"

function HomeBanner() {
    return (
        <div className={banner.container}>            
        <div className={banner.back}>           
        <div className={banner.back1}>
        <h1 className={banner.bannerHead}>
        Лучшие бани и сауны Костаная
        </h1>
        <div className={banner.box}>
        <div className={banner.box1}>
          <h5>Услуги</h5>
          <div className={banner.box2}>
            <p>Аренда бани или сауны</p>
            <div className={banner.icon}></div>
          </div>
        </div>
        <div className={banner.box1}>
          <h5>Услуги</h5>
          <div className={banner.box2}>
            <p>Продажа банных принадлежностей</p>
            <div className={banner.icon}></div>
          </div>
        </div>
        <div className={banner.box1}>
          <h5>Услуги</h5>
          <div className={banner.box2}>
            <p>Комнаты отдыха</p>
            <div className="icon"></div>
          </div>
        </div>
        <div className={banner.box1}>
          <h5>Услуги</h5>
          <div className={banner.box2}>
            <p>Большой зал</p>
            <div className={banner.icon}></div>
          </div>
        </div>
      </div>
        </div> 
        </div>       
        </div>
    );
}

export default HomeBanner;
