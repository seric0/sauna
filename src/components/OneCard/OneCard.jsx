import React from "react";
import { Link } from "react-router-dom";

import onecard from "./OneCard.module.scss";

function OneCard(props) {    
  return (        
    <div className={onecard.card}>              
      <div className={onecard.imageWrapper}>
        <img className={onecard.fill} src={props.img} alt="" />
      </div>
      <div className={onecard.data}>{props.name}</div>
      <div className={onecard.contacts}>
      <div className={onecard.contact}>{props.region}</div>
      <div className={onecard.contact}>{props.address}</div>
      </div>
      <div className={onecard.contacts}>        
      <div className={onecard.contact}>{props.number_halls}</div>
      <div className={onecard.contact}>{props.number_people}</div>
      </div>
      <div className={onecard.contact}>{props.types_sauna}</div>
      <div className={onecard.price}>Цена от {props.price} тенге</div>            
      <div className={onecard.text}>{props.description}</div>               
      <Link key={props.id} to={`/all/${props.id}`} className={onecard.button}>Подробнее</Link>                                                                      
    </div>       
  );
}

export default OneCard;
