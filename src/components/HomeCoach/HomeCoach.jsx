import React from "react";

import coach from "./HomeCoach.module.scss"

import man_logo from "../../images/man_logo.png";

import rectangle from "../../images/rectangle.png";

function HomeCoach() {    
    return (
        <div className={coach.container}>                            
                <p className={coach.main_text}>Продавайте свои услуги вместе с нами</p> 
                <p className={coach.text}>Размещайте свои бани на портале, публикуйте акции, увеличивайте продажи</p>                
                <img className={coach.fill} src={rectangle} alt="" />                
                <img className={coach.fill} src={man_logo} alt="" />                                                
        </div>
    );
}

export default HomeCoach;
