import React from "react";

import card from "./Card_404.module.scss";

import Error from "../../images/404.png";

function Card_404() {
  return (
    <div className={card.card}>      
        <img className={card.fill} src={Error} alt="" />      
      <p className={card.headtext}>Ошибка 404</p>      
      <div className={card.text}>
      Кажеться что-то пошло не так! Страница, которую вы запрашиваете, не существует. Возможно она устарела, была удалена, или был введен неверный адрес в адреснй строке.
      </div>
    </div>    
  );
}

export default Card_404;
