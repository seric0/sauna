import React, { useState } from "react";

import card from "./NewsCard.module.scss";

import Error from "../../images/404.png";

import { useDispatch } from "react-redux";

import { saveDateTime } from "../../store/postSlice";

function NewsCard(props) {
  const dispatch = useDispatch();
  let id = props.id;
  const [date, setDate] = useState("");  
  const onSubmit = (e) => {
    e.preventDefault();   
    dispatch(saveDateTime({id, date})); 
  };
  if (id!=null) {
  return (
    <div className={card.card}>
      <div className={card.data}>{props.name}</div>
      <div className={card.imageWrapper}>
        <img className={card.fill} src={props.img} alt={props.name} />        
      </div>            
      <div className={card.mainText}>
      <div className={card.address_text}>Адрес</div>
      <div className={card.contact}>{props.region}, {props.address}</div>      
      <div className={card.contacts}>      
      <div className={card.head}>{props.number_halls}</div>
      <div className={card.head}>{props.number_people}</div>
      <div className={card.head}>{props.types_sauna}</div>              
      <div className={card.head}>Цена от {props.price} тенге</div>
      <div className={card.head}>Телефон:</div>
      <div className={card.head}>{props.phone}</div>
      <form className={card.form} onSubmit={onSubmit}>
      <input type="datetime-local" id="localdate" name="date" className={card.date} onChange={(e) => setDate(e.target.value)}/>
      <button className={card.button} type="submit">Забронировать</button>
      </form>
      </div>
      <div className={card.hr}></div>
      <div className={card.detailed_text}>Описание бани/сауны</div>
      <div className={card.detailed_description}>{props.detailed_description}</div>                            
      <div className={card.text}>{props.description}</div>            
      <div className={card.hr}></div>
      <div className={card.services_text}>Услуги</div>
      <div className={card.services}>        
        <div>Прохладительные напитки.</div>      
        <div>Прохлодительный душ.</div>            
        <div>В некоторых банях и саунах: Бассейн с двойной системой очистки, а также душевые комнаты.</div>            
        <div>На территории бани и сауны есть кабельное телевидение.</div>      
        </div>         
      </div>           
    </div>    
  );
  }
  else {
    return (
    <div className={card.error_card}>      
        <img className={card.error_fill} src={Error} alt="" />      
      <p className={card.error_headtext}>Ошибка 404</p>      
      <div className={card.error_text}>
      Кажеться что-то пошло не так! Страница, которую вы запрашиваете, не существует. Возможно она устарела, была удалена, или был введен неверный адрес в адреснй строке.
      </div>
    </div> 
    );  
  }
}

export default NewsCard;
