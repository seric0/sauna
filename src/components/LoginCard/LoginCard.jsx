import React, { useState } from "react";

import { useDispatch } from "react-redux";

import { signIn } from "../../store/userSlice";

import { Link } from "react-router-dom";

import card from "./LoginCard.module.scss";

function LoginCard() {
  const dispatch = useDispatch();
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");

  const onSubmit = (e) => {
    e.preventDefault();   
    dispatch(signIn({login, password})); 
  };
  return (
    <div className={card.Myform}>
      <h1>Авторизация</h1>
      <div className={card.inp}>
      <form onSubmit={onSubmit}>
      <input className={card.log} type="text" required value={login} placeholder="логин" onChange={(e) => setLogin(e.target.value)}/>
      <input className={card.pass} type="password" required value={password} placeholder="пароль" onChange={(e) => setPassword(e.target.value)}/>
      <button className={card.btn} type="submit">Войти</button>
      <Link to={`/panel`} className={card.btn}>Войти</Link>      
      </form>   
      </div>
    </div>    
  );
}

export default LoginCard;
