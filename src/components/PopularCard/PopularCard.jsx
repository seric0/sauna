import React from "react";

import popular from "./PopularCard.module.scss";

import { Link } from "react-router-dom";

import SaunaCard_1 from "../../images/saunaCard_1.png";
import SaunaCard_2 from "../../images/saunaCard_2.png";
import SaunaCard_3 from "../../images/saunaCard_3.png";
import SaunaCard_4 from "../../images/saunaCard_4.png";

function PopularCard() {  
  const homecards = [
    {
      id: 42,
      img: SaunaCard_1,
      text: "Баня Рахат",
      price: '1500'
    },
    {
      id: 11,
      img: SaunaCard_2,
      text: "Баня сауна Аксу",
      price: '1500'
    },
    {
      id: 10,
      img: SaunaCard_3,
      text: "Баня сауна Эврика",
      price: '3000'
    },
    {
      id: 2,
      img: SaunaCard_4,
      text: "VIP сауна Лед",
      price: '7000'
    } 
  ];
  return (    
    homecards.map((homecard) => (
    <div className={popular.card}>        
      <div className={popular.imageWrapper}>
        {/* <img className={popular.fill} src={homecard.img} alt="" /> */}
        <Link key={homecard.id} to={`/all/${homecard.id}`} className={popular.text}><img src={homecard.img} alt="" /></Link>
      </div>
      {/* <div className={s.text}>{homecard.text}</div> */}
      <Link key={homecard.id} to={`/all/${homecard.id}`} className={popular.text}>{homecard.text}</Link>
      <div className={popular.price}>от {homecard.price} тенге</div>
    </div> 
    )) 
  );
}

export default PopularCard;
