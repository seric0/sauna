import React from "react";

import header from "./Header.module.scss"

function Header(props) {

    return (
        <header>
            <div className={header.container}>
                <div className={header.headerTop}>
                    <nav>
                        <div className={header.logo}>
                            <img src={props.img} alt="" />
                        </div>
                        <ul>
                            <li>
                                <a href="/"> {props.menu_1}</a>
                            </li>
                            <li>
                                <a href="/all"> {props.menu_2}</a>
                            </li>
                            <li>
                                <a href="/gallery"> {props.menu_3}</a>
                            </li>
                            <li>
                                <a href="/about"> {props.menu_4}</a>
                            </li>                            
                        </ul>
                        {/* <div className={header.icons}>
                            <img className="icon" src={YoutubeImage} alt="dots icon" />
                            <img className="icon" src={VkImage} alt="dots icon" />
                        </div> */}
                        <div className={header.headerMobileMenu}>---</div>
                        <div className={header.headerHr}></div>
                    </nav>
                </div>
            </div>
        </header>
    );
}

export default Header;
