import { createSlice } from "@reduxjs/toolkit";

const userCredentials = {
  login: "admin",
  password: "admin",
};

const userSlice = createSlice({
  name: "user",
  initialState: { user: userCredentials },
  reducers: {
    signIn(state, action) {
      const { login, password } = action.payload;
      if (
        userCredentials.login === login &&
        userCredentials.password === password
      ) {
        state.user = action.payload;
      }
    },
  },
});

export const { signIn} = userSlice.actions;
export default userSlice.reducer;
