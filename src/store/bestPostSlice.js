import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

const endpoint = process.env.REACT_APP_ENDPOINT || "";

export const getUsers = createAsyncThunk(
    "post/getPosts",
    async ({ limit, page, isExpanded }, { rejectWithValue }) => {
      try {
        let queryParams = "";        
        if (limit) {
          if (queryParams) queryParams += "&";
          queryParams += `_limit=${limit}`;
        }
        if (page) {
          if (queryParams) queryParams += "&";
          queryParams += `_page=${page}`;
        }
        if (isExpanded) {
          if (queryParams) queryParams += "&";
          queryParams += `_expand=user`;
        }
        
        const response = await fetch(
          `${endpoint}/product/read.php${queryParams ? `?${queryParams}` : ""}`
        );
  
        if (!response.ok) {
          throw new Error("Server error");
        }
  
        const data = await response.json();
  
        return data;
      } catch (error) {
        return rejectWithValue(error.message);
      }
    }
  );

const BestPostSlice = createSlice({
  name: "post",
  initialState: { post: [] },
  reducers: {        
  },
  extraReducers: {
    [getPosts.pending]: (state) => {
      state.isLoading = true;
    },
    [getPosts.rejected]: (state, action) => {
      state.isLoading = false;
      state.isError = true;
      state.error = action.payload;
    },
    [getPosts.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.isError = false;
      state.error = "";
      state.post = action.payload;
    },    
  },
});

export const { logOut } = BestPostSlice.actions;

export default BestPostSlice.reducer;
