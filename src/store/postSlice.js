import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

const endpoint = process.env.REACT_APP_ENDPOINT || "";

export const showPost = createAsyncThunk(
  "post/showPost",
  async function ({ name, description, price }, { rejectWithValue }) {
    try {
      const response = await fetch(
        `${endpoint}/product/read.php`
      );

      if (!response.ok) {
        throw new Error("Server error!");
      }

      const data = await response.json();

      if (data.length < 1) {
        throw new Error("There is no such post!");
      }      
      return data;      
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const savePost = createAsyncThunk(
  "post/savePost",
  async ({ name, price, description, detailed_description, region, address, number_halls, number_people, types_sauna, work_time, thumbnail,category_id }) => {
      return fetch(`${endpoint}/product/create.php`, {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({
            name,
            price,
            description,
            detailed_description,
            region,
            address,
            number_halls,
            number_people,
            types_sauna,
            work_time,
            thumbnail,
            category_id
          }),
      }).catch('Failed to save post!')
          .then((res) => res.json())
          .then((res) => res)
  },
)

export const saveDateTime = createAsyncThunk(
  "post/saveDateTime",
  async ({ id, rec_datetime}) => {
      return fetch(`${endpoint}/product/booking.php`, {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({
            id,
            rec_datetime            
          }),
      }).catch('Failed to save datetime!')
          .then((res) => res.json())
          .then((res) => res)
  },
)

export const getPosts = createAsyncThunk(
  "post/getPosts",
  async ({ userId, limit, page, isExpanded }, { rejectWithValue }) => {
    try {
      let queryParams = "";
      if (userId) {
        queryParams += `userId=${userId}`;
      }
      if (limit) {
        if (queryParams) queryParams += "&";
        queryParams += `_limit=${limit}`;
      }
      if (page) {
        if (queryParams) queryParams += "&";
        queryParams += `_page=${page}`;
      }
      if (isExpanded) {
        if (queryParams) queryParams += "&";
        queryParams += `_expand=user`;
      }
      
      // const response = await fetch(
      //   `${endpoint}/product/read.php${queryParams ? `?${queryParams}` : ""}`
      // );

      const response = await fetch(
        `${endpoint}/product/read.php`
      );

      if (!response.ok) {
        throw new Error("Server error");
      }

      const data = await response.json();

      return data;
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const deletePost = createAsyncThunk(
  "post/deletePost",
  async ({ id }) => {
      return fetch(`${endpoint}/posts/${id}`, {
          method: 'DELETE',
          headers: { 'Content-Type': 'application/json' },         
      }).catch('Failed to delete post!') 
        .then((res) => res.json())
        .then((res) => res)       
  },
)

const postSlice = createSlice({
  name: "post",
  initialState: {post: []},  
  extraReducers: {
    [showPost.pending]: (state) => {
      state.isLoading = true;
    },
    [showPost.rejected]: (state, action) => {
      state.isLoading = false;
      state.isError = true;
      state.error = action.payload;
    },
    [showPost.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.isError = false;
      state.error = "";
      state.post = action.payload;
    },
    [getPosts.pending]: (state) => {
      state.isLoading = true;
    },
    [getPosts.rejected]: (state, action) => {
      state.isLoading = false;
      state.isError = true;
      state.error = action.payload;
    },
    [getPosts.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.isError = false;
      state.error = "";
      state.post = action.payload;
    },
    [savePost.pending]: (state) => {
      state.isLoading = true;
    },
    [savePost.rejected]: (state, action) => {
      state.isLoading = false;
      state.isError = true;
      state.error = action.payload;
    },
    [savePost.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.isError = false;
      state.error = "";
      state.post = action.payload;
    },
    [saveDateTime.pending]: (state) => {
      state.isLoading = true;
    },
    [saveDateTime.rejected]: (state, action) => {
      state.isLoading = false;
      state.isError = true;
      state.error = action.payload;
    },
    [saveDateTime.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.isError = false;
      state.error = "";
      state.post = action.payload;
    },  
    [deletePost.pending]: (state) => {
      state.isLoading = true;
    },
    [deletePost.rejected]: (state, action) => {
      state.isLoading = false;
      state.isError = true;
      state.error = action.payload;
    },
    [deletePost.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.isError = false;
      state.error = "";
      state.post = action.payload;
    },   
  },
});

export const { addPost } = postSlice.actions;

export default postSlice.reducer;
