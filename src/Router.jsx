import React from "react";

import {BrowserRouter, Routes, Route} from "react-router-dom";

import { useSelector } from "react-redux";

import GalleryPage from "./pages/GalleryPage";

import AboutPage from "./pages/AboutPage";

import HomePage from "./pages/HomePage";

import NewsPage from "./pages/NewsPage";

import LoginPage from "./pages/LoginPage";

import PanelPage from "./pages/PanelPage";

import NewsCardPage from "./pages/NewsCardPage";

import Page_404 from "./pages/Page_404";

function Router() {
  const { user } = useSelector((state) => state.user);
  return (    
    <BrowserRouter>
    <Routes>    
      <Route path="/" element={<HomePage />} />
      <Route path="/gallery" element={<GalleryPage />} />
      <Route path="/about" element={<AboutPage />} />
      <Route path="/all" element={<NewsPage />} />
      <Route path="/all/:id" element={<NewsCardPage />} />
      <Route path="/admin" element={<LoginPage />} />
      {/* <Route path="/panel" element={<PanelPage />} /> */}
      {user && <Route path="/panel" element={<PanelPage />} />}
        {/* {!user && (
          <Route path="*" element={<LoginPage />} />
        )} */}
      <Route path="*" element={<Page_404 />} />
    </Routes>
    </BrowserRouter>
  );
}

export default Router;