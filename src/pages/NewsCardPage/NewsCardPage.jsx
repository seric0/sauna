import React, {useState, useEffect} from "react";

import { useParams } from "react-router-dom";
// import { useDispatch, useSelector } from "react-redux";
// import { getPosts } from "../../store/postSlice";
import NewsCard from "../../components/NewsCard/NewsCard";
import Header from "../../components/Header/Header";
import NewBanner from "../../components/NewBanner/NewBanner";
import Footer from "../../components/Footer/Footer";
import LogoImage from "../../images/logo.png";

import news from "./NewsCardPage.module.scss";

let headerMenu = [
  {
    id: 1,
    img: LogoImage,
    menu_1: "Главная",    
    menu_2:"Бани и сауны",        
    menu_3:"Галерея",    
    menu_4:"Контакты"
    }
];

function NewsCardPage() {    
  const { id } = useParams();
  // const dispatch = useDispatch();  
  const [post, setPosts] = useState([]);
    const endpoint = 'https://seric0.kz/API/Aggregator'
    useEffect(() => {
        const fetchData = async () => {            
            const res = await fetch(`${endpoint}/product/read_one.php/?id=${id}`)
            const data = await res.json()            
            setPosts(data)
            return data;
        }
        fetchData()
    }, [])
  return (
    <div className={news.swipper}>
      <div className={news.Header}>
        {headerMenu.map((linkName) => (
          <Header img={linkName.img} menu_1={linkName.menu_1} menu_2={linkName.menu_2} menu_3={linkName.menu_3}
            menu_4={linkName.menu_4} 
            key={linkName.id} />
        ))}
      </div>
      <div className={news.NewBanner}>
      <NewBanner/>      
      </div>
      <div className={news.list}>                        
          <NewsCard description={post.description} name={post.name} price={post.price} region={post.region} address={post.address} detailed_description={post.detailed_description} number_halls={post.number_halls} number_people={post.number_people} types_sauna={post.types_sauna} img={post.thumbnail} phone={post.phone} key={post.id} id={post.id}/>       
      </div>
      <div className={news.Footer}>
      {headerMenu.map((linkName) => (
              <Footer menu_1={linkName.menu_1} menu_2={linkName.menu_2} menu_3={linkName.menu_3}
              menu_4={linkName.menu_4} 
              key={linkName.id}/>      
            ))}      
      </div>   
    </div>
  )
}

export default NewsCardPage;