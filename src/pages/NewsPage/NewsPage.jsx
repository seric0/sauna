import React, { useState, useEffect } from "react";

import { useSelector, useDispatch } from 'react-redux';
import { getPosts } from "../../store/postSlice";

import OneCard from "../../components/OneCard";
import Header from "../../components/Header/Header";
import NewBanner from "../../components/NewBanner/NewBanner";
import Footer from "../../components/Footer/Footer";

import LogoImage from "../../images/logo.png";

import news from "./NewsPage.module.scss";

let headerMenu = [
  {
    id: 1,
    img: LogoImage,
    menu_1: "Главная",    
    menu_2:"Бани и сауны",        
    menu_3:"Галерея",    
    menu_4:"Контакты"
    }
];

function NewsPage() {  
  const { post } = useSelector((state) => state.post);  
  const [value, setValue] = useState(''); 
  console.log(post.products);
  const dispatch = useDispatch();
  const filteredBani = post.products?.filter(bani=> {
    return bani.name.toLowerCase().includes(value.toLowerCase());
  }); 
  useEffect(() => {
    dispatch(getPosts({  }));
  }, [dispatch]);    
  return (
    <div className={news.swipper}>
      <div className={news.Header}>
        {headerMenu.map((linkName) => (
          <Header img={linkName.img} menu_1={linkName.menu_1} menu_2={linkName.menu_2} menu_3={linkName.menu_3}
            menu_4={linkName.menu_4} 
            key={linkName.id} />
        ))}
      </div>
      <div className={news.NewBanner}>
      <NewBanner/>      
      </div>      
      <div className={news.Myform}>
      <h1>Поиск</h1>
      <div className={news.inp}>
      <form>
      <input className={news.log} type="text" required placeholder="Наименование" onChange={(e) => setValue(e.target.value)}/>      
      </form>   
      </div>
    </div>  
      <div className={news.list}>       
      {filteredBani?.map((card) => (        
          <OneCard name={card.name} description={card.description} img={card.thumbnail} region={card.region} address={card.address} number_halls={card.number_halls} number_people={card.number_people} types_sauna={card.types_sauna} price={card.price} key={card.id} id={card.id}/>          
          ))}   
      {/* {post?.products?.map((card) => (        
          <OneCard name={card.name} description={card.description} img={card.thumbnail} region={card.region} address={card.address} number_halls={card.number_halls} number_people={card.number_people} types_sauna={card.types_sauna} price={card.price} key={card.id} id={card.id}/>          
          ))}                        */}
      </div>      
      <div className={news.Footer}>
      {headerMenu.map((linkName) => (
              <Footer menu_1={linkName.menu_1} menu_2={linkName.menu_2} menu_3={linkName.menu_3}
              menu_4={linkName.menu_4} 
              key={linkName.id}/>      
            ))}
      </div>   
    </div>
  )
}

export default NewsPage;