import React from "react";

import Header from "../../components/Header/Header";

import s from "./OneNewsPage.module.scss";

function OneNewsPage() {
  return (
    <div>
      <Header />
    </div>
  );
}

export default OneNewsPage;
