import React from "react";

import PanelCard from "../../components/PanelCard/PanelCard";
import Header from "../../components/Header/Header";
import NewBanner from "../../components/NewBanner/NewBanner";
import Footer from "../../components/Footer/Footer";
import LogoImage from "../../images/logo.png";

import news from "./PanelPage.module.scss";

let headerMenu = [
  {
    id: 1,
    img: LogoImage,
    menu_1: "Главная",    
    menu_2:"Бани и сауны",        
    menu_3:"Галерея",    
    menu_4:"Контакты"
    }
];

function PanelPage() {     
  return (
    <div className={news.swipper}>
      <div className={news.Header}>
        {headerMenu.map((linkName) => (
          <Header img={linkName.img} menu_1={linkName.menu_1} menu_2={linkName.menu_2} menu_3={linkName.menu_3}
            menu_4={linkName.menu_4}
            key={linkName.id} />
        ))}
      </div>
      <div className={news.NewBanner}>
      <NewBanner/>      
      </div>
      <div className={news.list}>                       
          <PanelCard />        
      </div>
      <div className={news.Footer}>
      {headerMenu.map((linkName) => (
              <Footer menu_1={linkName.menu_1} menu_2={linkName.menu_2} menu_3={linkName.menu_3}
              menu_4={linkName.menu_4} 
              key={linkName.id}/>      
            ))}      
      </div>   
    </div>
  )
}

export default PanelPage;