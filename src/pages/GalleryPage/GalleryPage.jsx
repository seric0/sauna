import React from "react";

import Header from "../../components/Header/Header";
import NewBanner from "../../components/NewBanner/NewBanner";
import Footer from "../../components/Footer/Footer";

import Slider from "../../components/Slider";

import s from "./GalleryPage.module.scss"

import LogoImage from "../../images/logo.png";

let headerMenu = [
  {
    id: 1,
    img: LogoImage,
    menu_1: "Главная",    
    menu_2:"Бани и сауны",        
    menu_3:"Галерея",    
    menu_4:"Контакты"
    }
];

function GalleryPage() {  
  return (
    <div>
      <div className={s.Header}>
      {headerMenu.map((linkName) => (
              <Header img={linkName.img} menu_1={linkName.menu_1} menu_2={linkName.menu_2} menu_3={linkName.menu_3}
              menu_4={linkName.menu_4} 
              key={linkName.id}/>      
            ))}      
      </div>
      <div className={s.AboutClub}>
      <NewBanner/>      
      </div>
      <div className={s.SliderWrapper}>
      <Slider/>      
      </div>      
      <div className={s.Footer}>
      {headerMenu.map((linkName) => (
              <Footer menu_1={linkName.menu_1} menu_2={linkName.menu_2} menu_3={linkName.menu_3}
              menu_4={linkName.menu_4} 
              key={linkName.id}/>      
            ))}      
      </div>
      </div>    
  );
}

export default GalleryPage;
