import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from 'react-redux';
import { getPosts } from "../../store/postSlice";

import Header from "../../components/Header/Header";
import HomeBanner from "../../components/HomeBanner/HomeBanner";
import PopularCard from "../../components/PopularCard/PopularCard";
import StockCard from "../../components/StockCard/StockCard";
import HomeCoach from "../../components/HomeCoach/HomeCoach";
import Footer from "../../components/Footer/Footer";

import LogoImage from "../../images/logo.png";

import s from "./HomePage.module.scss"

let headerMenu = [
  {
    id: 1,
    img: LogoImage,
    menu_1: "Главная",    
    menu_2:"Бани и сауны",        
    menu_3:"Галерея",    
    menu_4:"Контакты"
    }
  ];

function HomePage() {  
  const { post } = useSelector((state) => state.post);
  const [page, setPage] = useState(1);  
  const dispatch = useDispatch();
  
  useEffect(() => {
    dispatch(getPosts({ limit: 9, page: page, isExpanded: true }));
  }, [dispatch]);
  return (
    <div>
      <div className={s.Header}>
      {headerMenu.map((linkName) => (
              <Header img={linkName.img} menu_1={linkName.menu_1} menu_2={linkName.menu_2} menu_3={linkName.menu_3}
              menu_4={linkName.menu_4} 
              key={linkName.id}/>      
            ))}      
      </div>
      <div className={s.HomeBanner}>
      <HomeBanner/>   
      </div>
      <div className={s.list}>
      <p className={s.text}>Популярные</p>            
            <PopularCard/>          
      </div>
      <div className={s.list}>
      <p className={s.text}>Акции и спецпредложения</p>      
            <StockCard/>           
      </div>
      <div className={s.HomeCoach}>
      <HomeCoach/>   
      </div>      
      <div className={s.Footer}>
      {headerMenu.map((linkName) => (
              <Footer menu_1={linkName.menu_1} menu_2={linkName.menu_2} menu_3={linkName.menu_3}
              menu_4={linkName.menu_4} 
              key={linkName.id}/>      
            ))}      
      </div>    
      </div>    
  );
}

export default HomePage;
